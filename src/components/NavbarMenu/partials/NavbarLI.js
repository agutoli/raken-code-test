import styled from 'styled-components'

export default styled.li`
  color: #4a4a4a;
  cursor: pointer;
  list-style: none;
  padding: 0 20px;
  font-family: Roboto;
  font-size: 14px;

  :hover {
    color: #f26620;
  }
`
