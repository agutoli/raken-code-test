import styled from 'styled-components'

export default  styled.nav`
  display: flex;
  align-items: center;
  position: relative;
  justify-content: space-between;
  width: auto;
  height: 76px;
  background-color: #f3f3f3;
  box-shadow: 0 0 4px 0 rgba(0, 0, 0, 0.25);
`
