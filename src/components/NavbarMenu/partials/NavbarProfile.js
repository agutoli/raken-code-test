import React from 'react'
import styled from 'styled-components'

import ProfileSVG from '../../../assets/svg/profile.svg'

import DropdownMenu from '../../DropdownMenu'

const NavbarProfile = styled.div`
  display: flex;
  cursor: pointer;
  align-items: center;
  justify-content: center;
  width: 100px;
  height: 75px;
  margin-left: 10px;
  border-left: 1px solid #e4e4e4;
  padding: 1px;
`

class NavbarMenu extends React.PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      isDropdownOpened: false
    }

    this.onMouseOutBinded = this.onMouseOut.bind(this)
    this.onMouseOverBinded = this.onMouseOver.bind(this)
  }

  onMouseOut() {
    this.timeout = setTimeout(() => {
      this.setState({
        isDropdownOpened: false
      })
    }, 100)
  }

  onMouseOver() {
    clearInterval(this.timeout)
    this.setState({
      isDropdownOpened: true
    })
  }

  renderDropdown() {
    if (this.state.isDropdownOpened) {
      return (
        <DropdownMenu items={this.props.items} />
      )
    }
    return null
  }

  render() {
    return (
      <NavbarProfile onMouseOver={this.onMouseOverBinded} onMouseOut={this.onMouseOutBinded}>
        <ProfileSVG width={50} />
        {this.renderDropdown()}
      </NavbarProfile>
    )
  }
}

export default NavbarMenu
