import React from 'react'
import PropTypes from 'prop-types'

import NavbarUL from './partials/NavbarUL'
import NavbarLI from './partials/NavbarLI'
import NavbarBrand from './partials/NavbarBrand'
import NavbarRight from './partials/NavbarRight'
import NavbarProfile from './partials/NavbarProfile'
import NavbarWrapper from './partials/NavbarWrapper'

const NavbarMenu = ({ items, dropdownItems }) => (
  <NavbarWrapper>
    <NavbarBrand>COMPANY NAME</NavbarBrand>
    <NavbarRight>
      <NavbarUL>
        {items.map(item => (<NavbarLI linkTo={item.linkTo}>{item.label}</NavbarLI>))}
      </NavbarUL>
      <NavbarProfile items={dropdownItems} />
    </NavbarRight>
  </NavbarWrapper>
)

NavbarMenu.propTypes = {
  items: PropTypes.array
}

NavbarMenu.defaultProps = {
  items: []
}

export default NavbarMenu
