import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
// import { linkTo } from '@storybook/addon-links';

import NavbarMenu from './NavbarMenu'

const items = [
  { label: "DASHBOARD", linkTo: "/" },
  { label: "PROJECTS", linkTo: "/" },
  { label: "TEAM", linkTo: "/" },
  { label: "COMPANY", linkTo: "/" }
]

const dropItems = [
  { label: "MY PROFILE", alerts: "23" },
  { label: "MY PROJECTS", alerts: "23" },
  { label: "NOTIFICATIONS", alerts: "23" },
  { label: "TASKS", alerts: "" },
  { label: "HELP", alerts: "" },
  { label: "LOGOUT", alerts: "" },
]

storiesOf('NavbarMenu', module)
  .add('default', () => (
    <NavbarMenu items={items} dropdownItems={dropItems} />
  ));
