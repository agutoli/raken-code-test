import React from 'react'
import PropTypes from 'prop-types'

import DropdownMenuLI from './partials/DropdownMenuLI'
import DropdownMenuUL from './partials/DropdownMenuUL'
import DropdownMenuWrapper from './partials/DropdownMenuWrapper'

const DropdownMenu = ({ items }) => (
  <DropdownMenuWrapper>
    <DropdownMenuUL>
      {items.map(item => <DropdownMenuLI {...item} />)}
    </DropdownMenuUL>
  </DropdownMenuWrapper>
)

DropdownMenu.propTypes = {
  items: PropTypes.array
}

DropdownMenu.defaultProps = {
  items: []
}

export default DropdownMenu
