import React from 'react'
import styled from 'styled-components'

const DropdownMenuLI = styled.li`
  cursor: pointer;
  overflow: hidden;
  color: #4a4a4a;
  list-style: none;
  padding: 0px 20px;
  font-family: Roboto;
  line-height: 17px;
  font-size: 14px;
  font-weight: bold;

  :hover {
    background-color: #d8d8d8;
    box-shadow: inset 0 1px 3px 0 rgba(0, 0, 0, 0.2);
  }

  :last-child div {
    border-bottom: none;
  }
`

const Text = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 20px 0;
  border-bottom: 1px solid #ececec;

  :hover {
    border-bottom: 1px solid transparent;
  }
`

const Alert = styled.span`
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 10px;
  width: 20px;
  height: 20px;
  background-color: #f26620;
  color: white;
`

export default (props) => {
  const alerts = props.alerts ? <Alert>2</Alert> : null
  return (
    <DropdownMenuLI>
      <Text>{props.label || props.children} {alerts}</Text>
    </DropdownMenuLI>
  )
}
