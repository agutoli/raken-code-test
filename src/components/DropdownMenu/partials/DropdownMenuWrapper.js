import styled from 'styled-components'

export default  styled.div`
  padding: 5px 0;
  border-radius: 10px;
  box-shadow: 0px 0px 14px rgba(0,0,0,0.3);
  width: 190px;
  position: absolute;
  top: 100px;
  right: 10px;
  background: white;

  :after,
  :before {
    bottom: 100%;
    left: 80%;
    border: solid transparent;
    content: " ";
    height: 0;
    width: 0;
    position: absolute;
    pointer-events: none;
  }

  :after {
    border-color: rgba(136, 183, 213, 0);
    border-bottom-color: white;
    border-width: 15px;
    margin-left: -15px;
  }

  :before {
    border-color: rgba(194, 225, 245, 0);
    border-bottom-color: #eee;
    border-width: 16px;
    margin-left: -16px;
  }

`
