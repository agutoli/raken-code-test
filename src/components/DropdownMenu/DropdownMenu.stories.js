import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import DropdownMenu from './DropdownMenu'

const items = [
  { label: "MY PROFILE", linkTo: "/" },
  { label: "MY PROJECTS", linkTo: "/" },
  { label: "NOTIFICATIONS", linkTo: "/" },
  { label: "TASKS", linkTo: "/" },
  { label: "HELP", linkTo: "/" },
  { label: "LOGOUT", linkTo: "/" },
]

storiesOf('DropdownMenu', module)
  .add('default', () => (
    <DropdownMenu items={items} />
  ));
